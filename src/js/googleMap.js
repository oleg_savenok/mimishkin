function initMap() {
	var styleArray = [
    {
      "featureType": "poi",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    }
  ];

	var map = new google.maps.Map(document.getElementById('map'), {
		center: {
			lat: 55.690303,
      lng: 37.657185
		},
		zoom: 16,
		styles: styleArray,
		disableDefaultUI: true,
    zoomControl: true,
    zoomControlOptions: {
      position: google.maps.ControlPosition.LEFT_BOTTOM
    }
  });

  var infowindow = new google.maps.InfoWindow({});

  var markerHome = new google.maps.Marker({
    position: {
      lat: 55.690303,
      lng: 37.657185
    },
    map: map,
    icon: 'img/marker.png'
  });

  markerHome.addListener('mouseover', function () {
    infowindow.setContent('<h5>Детский сад "Мимишкин"</h5>');
    infowindow.open(map, this);
  });
  markerHome.addListener('mouseout', function () {
    infowindow.close();
  });
}

window.initMap = initMap;