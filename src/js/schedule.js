$(function() {
	const containerSlider = $('.slider-container');
	const slider = $('.slider-container .slider');
	const singleSlide = $('.slider .single-slide');

	let currentSlide = 1;
	let containerSliderWidth = containerSlider.width();
	let numberOfSlides = (slider.find('.single-slide')).length;

	function changeSlide(slideNumber) {
		if (slideNumber > 0 && slideNumber <= numberOfSlides) {
			slider.css({
				'marginLeft': - ((containerSliderWidth * slideNumber) - containerSliderWidth)
			});
			currentSlide = slideNumber;

			activeCircleLink();
		};
	};

	function sizeParameters() {
		containerSliderWidth = containerSlider.width();

		slider.css({
			width: containerSliderWidth * numberOfSlides
		});

		singleSlide.css({
			width: containerSliderWidth
		});
	};

	function activeCircleLink() {
		if (currentSlide <= numberOfSlides / 2) {
			$('.left-link').children().removeClass('active');
			$('.right-link').children().removeClass('active');
			$('.left-link').children().eq(currentSlide - 1).addClass('active');
		} else {
			$('.right-link').children().removeClass('active');
			$('.left-link').children().removeClass('active');
			$('.right-link').children().eq((currentSlide - numberOfSlides / 2) - 1).addClass('active');
		}
	};

	// Action

	$(window).resize(function() {
		sizeParameters();
		changeSlide(currentSlide);
	});

	$('.controls .prev').click(function (e) {
		e.preventDefault();
		changeSlide(currentSlide - 1);
	});

	$('.controls .next').click(function (e) {
		e.preventDefault();
		changeSlide(currentSlide + 1);
	});

	$('.left-link .circle').click(function() {
		var index = $(this).index();
		changeSlide(index + 1);
	});

	$('.right-link .circle').click(function() {
		var index = $(this).index() + numberOfSlides / 2;
		changeSlide(index + 1);
	});

	// Call function

	activeCircleLink();
	sizeParameters();
});