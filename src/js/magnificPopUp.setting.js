$('.item').magnificPopup({
	type: 'image',
	closeOnContentClick: false,
	closeBtnInside: false,
	mainClass: 'mfp-fade',
	fixedContentPos: true,
	fixedBgPos: true,
	image: {
		verticalFit: true,
	},
	gallery: {
		enabled: true
	},
	zoom: {
		enabled: true,
		duration: 300
	}
});