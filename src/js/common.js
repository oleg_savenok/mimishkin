$(function() {
	let windowHeight = $(window).height();
	const hamburger = $('.hamburger-wrapper');
	const nav = $('.nav');

	const lessonBlock = $('.lessons .block');
	const lessonImg = $('.lessons .image');

	const logoWhite = $('.logo.white');
	const logoBlack = $('.logo.black');

	function navActionScroll() {
		if ( $(window).width() > 1200 ) {
			if ( $(document).scrollTop() > (windowHeight / 6) ) {
				nav.removeClass('open');
			} else {
				nav.addClass('open');
			}
		}
	};

	function navMobileActionScroll() {
		if ( $(document).scrollTop() > 0 ) {
			nav.removeClass('open');
		}
	};

	hamburger.click(function() {
		if (nav.hasClass('open')) {
			nav.removeClass('open');
		} else {
			nav.addClass('open');
		}
	});

	function headerColor() {
		if ( $(document).scrollTop() > (windowHeight - 75) ) {
			nav.removeClass('white');
			nav.addClass('black');
		} else {
			nav.addClass('white');
			nav.removeClass('black');
		}
	}

	function lessonsHeight() {
		if ( $(window).width() > 1200 ) {
			lessonBlock.css({
				'height': lessonImg.height()
			});
		} else {
			lessonBlock.css({
				'height': 'auto'
			});
		}
	};

	// Action

	$(document).scroll(function() {
		navMobileActionScroll();
		navActionScroll();
		headerColor();
	});

	$(window).resize(function() {
		windowHeight = $(window).height();
		lessonsHeight();
	});

	// Call function

	navMobileActionScroll();
	navActionScroll();
	headerColor();
	lessonsHeight();
});