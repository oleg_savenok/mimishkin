$(function() {
	$('.comments-slider').bxSlider({
		prevSelector: '.prev-slide',
		nextSelector: '.next-slide',
		controls: true,
		pager: false,
		prevText: "",
		nextText: "",
		minSlides: 1,
		maxSlides: 1,
		moveSlides: 1,
		//maxHeight: 300,
		adaptiveHeight: false,
		slideWidth: 5000,
		slideMargin: 10,
		infiniteLoop: true,
		easing: 'easeOutQuint'
	});
});