$(function() {
	const header = $('.header');
	const hello = $('.hello');
	const darken = $('.parallax-darken');

	function headerParallax() {
		let scrollTop = $(window).scrollTop();

		header.css({
			'top': - scrollTop / 10
		});

		darken.css({
			'opacity': (scrollTop / $(window).height()) * .75
		});

		hello.css({
			'margin-top': scrollTop / 15
		});
	}

	$(window).scroll(function() {
		headerParallax();
	});

	$(window).resize(function() {
		headerParallax();
	});

	headerParallax();
});