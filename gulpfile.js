var gulp = require('gulp'),
  sass = require('gulp-sass'),
  browserSync = require('browser-sync'),
  autoprefixer = require('gulp-autoprefixer'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  cssnano = require('gulp-cssnano'),
  sourcemaps = require('gulp-sourcemaps'),
  browserify = require('gulp-browserify'),
  clean = require('gulp-clean');
  imagemin = require('gulp-imagemin');

gulp.task('css', function () {
  return gulp.src('src/styles/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('public/css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/css'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('css-min', function () {
  return gulp.src('src/styles/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 3 version'))
    .pipe(cssnano({
      zindex: false,
      reduceIdents: false
    }))
    //.pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('public/css'))
});

gulp.task('js', function () {
  gulp.src('src/js/**.js')
    .pipe(sourcemaps.init())
    .pipe(browserify({
      insertGlobals : true,
      debug : true
    }))
    .pipe(gulp.dest('public/js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/js'))
    .pipe(browserSync.reload({stream: true, once: true}));
});

gulp.task('js-min', function () {
  gulp.src('src/js/**.js')
    .pipe(browserify({
      insertGlobals : true,
      debug : true
    }))
    .pipe(uglify())
    //.pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('public/js'))
});

gulp.task('assets', function () {
  gulp.src(['src/assets/**/*'])
    .pipe(gulp.dest('public/assets'));
});

gulp.task('img', function () {
  gulp.src(['src/img/**/*'])
    .pipe(gulp.dest('public/img'));
});

gulp.task('img-min', function () {
  gulp.src(['src/img/**/*'])
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5})
    ]))
    .pipe(gulp.dest('public/img'));
});

gulp.task('pages', function () {
  gulp.src(['src/index.html'])
    .pipe(gulp.dest('public'));
});

gulp.task('mail', function () {
  gulp.src(['src/mail.php'])
    .pipe(gulp.dest('public'));
});

gulp.task('browser-sync', function () {
  browserSync.init(null, {
    server: {
      baseDir: "public"
    },
    reloadDelay: 50
  });
});

gulp.task('bs-reload', function () {
  browserSync.reload();
});

gulp.task('watch', ['assets', 'img', 'css', 'js', 'pages', 'browser-sync', 'mail'], function () {
  gulp.watch("src/styles/*/*.scss", ['css']);
  gulp.watch("src/index.html", ['pages', 'bs-reload']);
  gulp.watch("src/js/*.js", ['js']);
  gulp.watch("src/assets/**/*", ['assets', 'bs-reload']);
  gulp.watch("src/img/**/*", ['img', 'bs-reload']);
  gulp.watch("src/mail.php", ['mail', 'bs-reload']);
});

gulp.task('build', ['assets', 'img-min', 'css-min', 'js-min', 'pages', 'mail']);
